import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ConfigService} from "./config.service";
import {ApiService} from "./api.service";

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private configService: ConfigService, private apiService: ApiService) {
	}

	public githubInit(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/github/init`,
			params: {
				redirect_uri: this.configService.redirectUri
			}
		})
	}

	public githubLogin(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/github/login`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		})
	}

	public githubSignUp(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/github/signup`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		})
	}

	public githubContinue(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/github/continue`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		})
	}
}
