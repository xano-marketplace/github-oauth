import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {Router} from "@angular/router";
import {DOCUMENT} from "@angular/common";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null)
	public redirectUri: string;

	public config: XanoConfig = {
		title: 'GitHub Oauth Extension',
		summary: 'This extension provides functionality to enable authentication against a GitHub account using Xano as your backend.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/github-oauth',
		descriptionHtml: `
                <h2>Description</h2>
                <p>This extension supports three modes of authentication that can be leveraged for different requirements on your frontend application. Additional schema is merged into your user table to store necessary information in the github_oauth object.</p>
                <h2>Sign In with GitHub</h2>
                <p>This mode allow allows you to login to your application with your GitHub account. If you did not sign up previously through your GitHub account, then this API will fail. This API request is normally used in conjunction with the Sign up request.</p>
                <h2>Sign Up with GitHub </h2>
                <p>This mode allow allows you to sign up to your application with your GitHub account. This request will only work once for your user as it throws an error if you have a customer that has already signed up previously with this same request. If you have special requirements like perhaps an invite code, then this request tends to be more flexible than the "continue with GitHub" version..</p>
                <h2>Continue with GitHub</h2>
                <p>This mode is the most flexible because it allows both sign up and login in the same API request. If you want an extremely low friction entry point and don't have special sign up requirements, this is the best way to create a seamless experience for your customer.</p>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/oauth/github/signup',
			'/oauth/github/init',
			'/oauth/github/continue',
			'/oauth/github/login'
		]
	};

	constructor(private apiService: ApiService, private xanoService: XanoService, private router: Router, @Inject(DOCUMENT) private document) {
		const baseUrl = document.location.origin;
		this.redirectUri = baseUrl.includes('localhost') ? baseUrl + '/oauth/github' : baseUrl + '/github-oauth/oauth/github';

		this.xanoApiUrl.next(localStorage.getItem('xano_api_url'));
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
